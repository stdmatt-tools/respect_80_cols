#!/usr/bin/env python
##~---------------------------------------------------------------------------##
##                        _      _                 _   _                      ##
##                    ___| |_ __| |_ __ ___   __ _| |_| |_                    ##
##                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   ##
##                   \__ \ || (_| | | | | | | (_| | |_| |_                    ##
##                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   ##
##                                                                            ##
##  File      : main.py                                                       ##
##  Project   : respect_80_cols                                               ##
##  Date      : Feb 09, 2019                                                  ##
##  License   : GPLv3                                                         ##
##  Author    : stdmatt <stdmatt@pixelwizards.io>                             ##
##  Copyright : stdmatt 2019, 2020                                            ##
##                                                                            ##
##  Description :                                                             ##
##    Make the file comply with 80 columns rule.                              ##
##---------------------------------------------------------------------------~##


##----------------------------------------------------------------------------##
## Imports                                                                    ##
##----------------------------------------------------------------------------##
import sys;
import os;
import os.path;
import tempfile;
import pdb;

##----------------------------------------------------------------------------##
## Constants                                                                  ##
##----------------------------------------------------------------------------##
PROGRAM_NAME = "respect-80-columns";
PROGRAM_VERSION = "1.1.0";
PROGRAM_COPYRIGHT = "2019, 2020";

MAXIMUM_COLUMN_COUNT = 80;

##----------------------------------------------------------------------------##
## Functions                                                                  ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
def show_help(exit_code=0):
    msg = """Usage:
  {program_name} [--help] [--version]
  {program_name} [--in-place] <filenames>

  *--help    : Show this screen.
  *--version : Show program version and copyright.

  --in-place: Modify the file in place, otherwise print the contents to stdout.

Notes:
  --in-place option apply to all filenames that are given to the program.

  Options marked with * are exclusive, i.e. the {program_name} will run that
  and exit after the operation.
    """;
    print(msg.format(program_name=PROGRAM_NAME));
    exit(exit_code)


##------------------------------------------------------------------------------
def show_version():
    msg = """{program_name} - {program_version} - stdmatt <stdmatt@pixelwizards.io>
Copyright (c) {program_copyright_years} - stdmatt
This is a free software (GPLv3) - Share/Hack it
Check http://stdmatt.com for more :)""".format(
        program_name=PROGRAM_NAME,
        program_version=PROGRAM_VERSION,
        program_copyright_years=PROGRAM_COPYRIGHT
    );

    print(msg);
    exit(0);

##------------------------------------------------------------------------------
def fatal(s):
    print("[FATAL] " + s);
    exit(1);

##------------------------------------------------------------------------------
def write(f, *args):
    for a in args:
        f.write(a);

##------------------------------------------------------------------------------
def parse_cmd_line(args):
    is_in_place = False;
    filenames   = [];
    for a in args:
        if(a in ["-h", "--help"]):
            show_help();
        elif(a in ["-v", "--version"]):
            show_version();
        elif(a in ["-i", "--in-place"]):
            is_in_place = True;
        else:
            filenames.append(a);

    return is_in_place, filenames;


##----------------------------------------------------------------------------##
## Script                                                                     ##
##----------------------------------------------------------------------------##
def main(args):
    in_place, filenames = parse_cmd_line(args);

    for filename in filenames:
        ## @TODO(stdmatt): We should canonize the path instead...
        abs_filename = os.path.abspath(filename);

        ##
        ## Read the file contents.
        lines = [];
        try:
            f     = open(abs_filename);
            lines = f.readlines();

            f.close();
        except Exception as e:
            fatal("Can't open file - Path:({0})".format(abs_filename));

        out_file = sys.stdout;
        if(in_place):
            out_file = tempfile.NamedTemporaryFile(mode="w", delete=False);

        ##
        ## Check if we are ok for each line.
        for line in lines:
            line       = line.replace("\n", ""); ## Prevent the double new line.
            curr_count = 0;
            for word in line.split(" "):
                word_len = len(word);
                if(curr_count + word_len <= MAXIMUM_COLUMN_COUNT):
                    write(out_file, word, " ");
                    curr_count += word_len + 1;
                else:
                    write(out_file, "\n", word, " ");
                    curr_count = 1;

            write(out_file, "\n");

        if(in_place):
            out_file.close();
            os.rename(out_file.name, abs_filename);


if __name__ == "__main__":
    main(sys.argv[1:]);
